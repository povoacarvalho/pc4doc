# PC4Doc - Living documentation library

## Introduction

Goal of this library is to permit user to document it's code so some documentation
will be generated when building.

## Roadmap

The 0.0.1 release will contains some candidate annotations that I will test on another
project. As I will have enough experience on using this annotations a new release will
be done with not only annotations but also tools to generate the documentation.

## Warning
All annotations on 0.0.1 release are candidate annotations, that means they can evolve 
or disapear in future releases of the library.

## Linking regular code to specification

This library contains 2 annotations to document the link between code and specification
+ ImplementsStories : this annotation is to be placed on a service or on a method wich is the
entry point to the code written to implements one or more stories :

     Example :
     
     ```java
     @Service
     public class UserRegistration {
         @ImplementsStories("US-005")
         public void registerUser(RegisterUser command) {
             // ...
         }
  
         @ImplementsStories("US-006")
         public void unregisterUser(UnregisterUser command) {
             // ...
         }
     }
     ```
  
+ ImplementsBusinessRules : this annotation is to be placed on a method designed to
implement one or more business rules :

    Example :
    
     ```java
     @Service
     @ImplementsStories({"US-005", "US-006"})
     public class UserRegistration {
         @ImplementsBusinessRules({
             @RuleRef(story="US-005", rule="1"),
             @RuleRef(story="US-005", rule="3")
         })
         public void registerUser(RegisterUser command) {
             // ...
         }
     }
     ```

**Note : as @RuleRef contains the US ID, it is recommended not to mix on same method
annotations of type @ImplementsStories and @ImplementsBusinessRules. Usage of second
annotation is sufficient.**

## Linking testing code to specification

This library contains 3 annotations to create a link between the test code and the part
of the specification it checks :

+ TestStories : this annotation can be used on TestCase or on a method and means that the
test case or the method is designed to test one or more US.

    Example :

     ```java
     @TestStories("US-005")
     public class UserRegistrationTests {
         // ...
     }
     ```
         
+ TestBusinessRules : this annotation can be used on a method to document this method checks
one or more business rules.

    Example :

     ```java
     @TestStories("US-005")
     public class UserRegistrationTests {
         @Test
         @TestBusinessRules(@RuleRef(story="US-005", rule="1"))
         public void Given_something_When_an_action_Then_an_event() {
             // ...
         }
     }
     ```

+ TestAcceptanceCriterias : this annotation can be used on a method to document this method checks
one or more specific acceptance criterias.

    Example :

     ```java
     @TestStories("US-005")
     public class UserRegistrationTests {
         @Test
         @TestAcceptanceCriterias(@AcceptanceCriteriaRef(story="US-005", criteria="1"))
         public void Given_something_When_an_action_Then_an_event() {
             // ...
         }
     }
     ```

## Documenting very restricted usages

There's some scenarios with some frameworks where you must create a method or
a constructor but you don't want your developper to use it. A good example is
the non args constructor wich is needed by JPA, but we don't want developper to
use it on a DDD approach as it creates objects with a theorically impossible state.
This library contains 3 annotations to document such cases :
+ OnlyForJpa : used to document something which exists only for JPA and must not
be used by developers
+ OnlyForSerialization : used to document something which exists only for serialization
and must not be used by developers
+ OnlyForTests : used to document something only designed for testing purposes and 
must not be used on regular code.
 
## Documenting particular stereotypes

Currently only one annotation to document stereotypes is present in the library :
+ UtilityClass to mark an utility class. An utility class contains no state and
only static methods.