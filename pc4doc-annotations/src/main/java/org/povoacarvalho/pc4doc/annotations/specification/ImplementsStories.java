package org.povoacarvalho.pc4doc.annotations.specification;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * This annotation is used to mark the entry point in the code written to implement one or more specific stories.
 *
 * @author Idalecio LOPES
 * @version 1.0
 * @since PC4DOC 1.0
 */
@Documented
@Target({METHOD, TYPE})
@Retention(SOURCE)
public @interface ImplementsStories {
    /**
     * The IDs of the stories implemented.
     *
     * @return the IDs of the stories implemented.
     *
     * @since ImplementsStories 1.0
     */
    String[] value();
}
