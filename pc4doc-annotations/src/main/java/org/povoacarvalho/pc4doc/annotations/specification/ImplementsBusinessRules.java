package org.povoacarvalho.pc4doc.annotations.specification;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * This annotation is used to mark a method or a service to document that it is designed to implement one or more
 * business rules.
 *
 * @author Idalecio LOPES
 * @version 1.0
 * @since PC4DOC 1.0
 */
@Documented
@Target({METHOD, TYPE})
@Retention(SOURCE)
public @interface ImplementsBusinessRules {
    RuleRef[] value();
}
