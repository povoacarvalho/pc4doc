/**
 * This package contains annotations designed to document links between code and specifications.
 */
package org.povoacarvalho.pc4doc.annotations.specification;