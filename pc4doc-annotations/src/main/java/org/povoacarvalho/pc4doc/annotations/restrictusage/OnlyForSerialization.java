package org.povoacarvalho.pc4doc.annotations.restrictusage;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * This annotation is used to mark a constructor, a method or a field designed to be
 * used only by serialization. Examples of such targets are the serialUIDField, should be the
 * non args constructor, ...
 *
 * @author Idalecio LOPES
 * @since PC4DOC 1.0
 */
@Documented
@Target({TYPE, CONSTRUCTOR, FIELD, METHOD})
@Retention(SOURCE)
public @interface OnlyForSerialization {
}
