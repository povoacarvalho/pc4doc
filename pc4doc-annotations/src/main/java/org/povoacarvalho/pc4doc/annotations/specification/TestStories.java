package org.povoacarvalho.pc4doc.annotations.specification;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * This annotation is used to mark a method on a test case to document the fact this method is used to check a specific
 * user story.
 *
 * @author Idalecio LOPES
 * @version 1.0
 * @since PC4DOC 1.0
 */
@Documented
@Target(METHOD)
@Retention(SOURCE)
public @interface TestStories {
    /**
     * The IDs of the stories tested.
     *
     * @return the IDs of the stories tested.
     *
     * @since TestStories 1.0
     */
    String[] value();
}
