package org.povoacarvalho.pc4doc.annotations.specification;

/**
 * This annotation is used to mark a method in a test case to document that this method is used to check one or more
 * business rule.
 *
 * @author Idalecio LOPES
 * @version 1.0
 * @since PC4DOC 1.0
 */
public @interface TestBusinessRules {
    RuleRef[] value();
}
