package org.povoacarvalho.pc4doc.annotations.stereotype;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * This method is used to mark an utility class.
 *
 * @author Idalecio LOPES
 * @version 1.0
 * @since PC4DOC 1.0
 */
@Documented
@Target(TYPE)
@Retention(SOURCE)
public @interface UtilityClass {
}
