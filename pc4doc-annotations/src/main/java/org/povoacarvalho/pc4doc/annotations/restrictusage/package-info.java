/**
 * This package contains annotations designed only to document some elements designed only to be used on specific
 * use cases (tests, serialization, ...).
 */
package org.povoacarvalho.pc4doc.annotations.restrictusage;