package org.povoacarvalho.pc4doc.annotations.specification;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * This annotation is designed to define a rule reference. This annotation will be used inside ImplementsRules or
 * TestBusinessRules annotations.
 *
 * @author Idalecio LOPES
 * @version 1.0
 * @since PC4DOC 1.0
 * @see ImplementsBusinessRules
 * @see TestBusinessRules
 */
@Documented
@Target(METHOD)
@Retention(SOURCE)
public @interface RuleRef {
    /**
     * The ID of the story containing the rule.
     *
     * @return the ID of the story containing the rule.
     *
     * @since RuleRef 1.0
     */
    String story();

    /**
     * The ID of the rule.
     *
     * @return the ID of the rule.
     *
     * @since RuleRef 1.0
     */
    String rule();
}
