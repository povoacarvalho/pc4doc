/**
 * This package contains annotations designed to document stereotypes and patterns.
 */
package org.povoacarvalho.pc4doc.annotations.stereotype;